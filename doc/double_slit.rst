

.. _double_slit_section_label:

***********************************
Double slit Aperture
***********************************



Theoretical formalism
**************************

As a reminder of the section :ref:`diffraction_section_label`, the general expression of the amplitude at the position P in the Fourier plane is 

.. math::
    \Psi(P) =
	 \Psi(u,v) =
	 \int_{\Sigma}t(x,y)\underline{\Psi_{0}^{-}}e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma

with :math:`t(x,y)` the transmittance function, :math:`(u, v)` the spatial frequency wave numbers, :math:`\underline{\Psi_{0}^{-}}` the plane wave amplitude just before the holed screen and :math:`\mathrm{d}\Sigma` is the area of this screen.


For a double slit aperture the transmittance function :math:`t(x,y)` is defined as

.. math::
	t(x,y)= \left\lbrace
		\begin{aligned}
		&1 \quad\text{if}\quad -\frac{d}{2}-\frac{a}{2}< x  <-\frac{d}{2}+\frac{a}{2}\quad\text{and}\quad \vert y \vert <\frac{b}{2}\\
		&1 \quad\text{if}\quad \frac{d}{2}-\frac{a}{2}< x  <\frac{d}{2}+\frac{a}{2}\quad\text{and}\quad \vert y \vert <\frac{b}{2}\\
		&0 \quad\text{else}
		\end{aligned}\right.

where *a* is the dimension of each slit along the Ox direction, *b* the dimension along Oy and *d* the spacing between the slits.

This definition determines the upper and lower integration limits for the integrals along the *x* and *y* directions.

Usually, we suppose :math:`b \gg a` thereby we explain the amplitude as a function of *x* only

.. math::
	\Psi(u) =
	2\underline{\Psi_{0}^{-}}a\,\text{sinc}(ua)\cos(\pi ud)

with :math:`u=\frac{x_\text{i}}{\lambda f}`.
	
However, it is possible to include the diffraction along Oy which strongly contract the 2D diffraction map (user can add this phenomenon by clicking on the graphic interface).
In that case, the amplitude depends also of the spatial frequency *v* as

.. math::
	\Psi(u,v) =
	2\underline{\Psi_{0}^{-}}ab\,\text{sinc}(ua)\cos(\pi ud)\text{sinc}(vb)

with as a reminder :math:`v=\frac{y_\text{i}}{\lambda f}`.

Without the y part, the intensity is simply the modulus squared of :math:`\Psi(u)`

.. math::
	I(u) = 
	I_0\,\text{sinc}^2(ua)\cos^2(\pi ud)
	
where :math:`I_0=4\vert\underline{\Psi_{0}^{-}}\vert^2 a^2`.	
			  
The :math:`\text{sinc}^2(ua)` term describes the diffraction of an individual slit (without the second one) and :math:`\cos^2(\pi ud)` describes interferences between the waves coming from each of the slits.

Thus, the file <https://gitlab.com/diffraction_qt/diffraction_qt/-/blob/master/diffraction/diffraction_double_slit.py>`_ allows to study how the different parameters of the experiment modify the diffraction diagram.


Graphic interface description
=========================================

For the double slit configuration, the graphic interface is divided into five parts (see next Fig. :numref:`graphic_interface_2_label`).

.. _graph_interface_2_label:

.. figure:: _static/full_interface_double_slit.png
	:name: graphic_interface_2_label
	
	Graphic interface description

..  :align: center

Zone (a) contains all variables that can be modified by the user, (b) is a 2D map of the aperture qualitatively showing the double slit shape depending of *a*, *b* and the spacing *d*, 
(c) represents the 2D diffraction pattern map corresponding to the aperture and the graph on (d) represents the normalized intensity along the Oy=0 axis.
Curve (d) allows to observe the number of oscillations of *I* between two zeros of the sinc function.
Window (e) is a zoom of (d). We focus on the *I=[0; 0.05]* range to observe the small oscillations and observe the modulation by the sinc and the cosine functions. 
To clearly observe the cosine oscillation, we multiply this function by a factor 0.04 to reduce its amplitude. 
Before giving some examples, we will first describe the variables area. 



Variables area
=========================================
.. _var_area_2_label:

.. figure:: _static/variables_area_double_slit.png
	:name: variables_area_2_label

	Zoom on the "Variables" window

..  :align: center

Explanation of each parameter:

* **a** : dimension of the aperture along Ox (in micrometers)
* **b** : dimension of the aperture along Oy (as a multiple of a)
* **d** : spacing between the two slits along Ox (as a multiple of a)
* **lamda** : laser wavelength (in nanometers)
* **focal** : lens focal length (in centimeters)
* **NX** : numerical step to increase the pixel resolution of the curves in (c), (d) and (e) (high NX, high resolution, high calculation time), if the variable NX is too small, the cosine function does not go correctly to 0 at each oscillation 
* **NY** : same as NX
* **High Contrast** : increases the contrast of the 2D diffraction pattern map by reducing the upper limit of the color bar from 1 to 0.1
* **Diffraction along Oy** : modifies the 2D diffraction pattern map to add the diffraction along Oy direction (contraction of the figure along Oy as observed in real experiment)
* **plot sinc^2(ua)** : plot the sinc function modulation on the figure (e)
* **plot cos^2(ud)** : plot the cosine function modulation time 0.04 on the figure (e) (if we do not multiply by 0.04 we do not see the oscillations)
* **Plot button** : starts the calculation

 
Effect of a, b, d, lamda and focal
***********************************
The following image (:numref:`aperture_shape_fct_d_label`) shows the evolution of the double slit when we increase d from :math:`5a` to :math:`20a`. 

.. _ap_shape_fct_d_label:

.. figure:: _static/aperture_function_d.png
	:name: aperture_shape_fct_d_label

	Evolution of the double slit aperture as a function of d

..  :align: center


The effect of the other physical parameters was studied in the section :ref:`rectangular_section_label`.

Nevertheless, the usefulness of the variables NX and NY must be specified. On the following image (:numref:`discrt_NX_label`), we represent the diffraction curve for a small value (top panel) and a high value (bottom panel) of NX.
We observe that a high value of NX gives a better representation of the cosine oscillation.

.. _disc_NX_label:

.. figure:: _static/effect_discretisation_step_on_curves.png
	:name: discrt_NX_label

	Effect of NX on the rendering of the graphs (d) and (e)

..  :align: center

Different check boxes
***********************
The **High contrast** checkbox allows to increase the contrast on the diffraction 2d map by modifying the upper color bar limit (see section :ref:`rectangular_section_label` for example).

The **Diffraction along Oy** box adds the diffraction phenomenon along the Oy axis. Thus, the 2D diffraction pattern is shrank around the Ox axis (see :numref:`checkBox_Oy_label`).  

.. _checkbx_Oy_label:

.. figure:: _static/effet_chek_box_diffraction_along_Oy.png
	:name: checkBox_Oy_label
	
	Effect of the **Diffraction along Oy** check box on the diffraction 2D map

..  :align: center

The **plot sinc^2(ua)** and **plot cos^2(ud)** checkboxes allows to represent the two modulations imposed by the sinc and cosine functions (see :numref:`checkBox_plot_label`).

.. _checkbx_plot_label:

.. figure:: _static/different_plot_config_zoom.png
	:name: checkBox_plot_label

	Effect of the **plot sinc^2(ua)** and **plot sinc^2(ua)** check boxes on the graph (e)

..  :align: center