.. diffraction_qt documentation master file, created by
   sphinx-quickstart on Wed Oct 14 11:51:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

   
.. figure:: _static/logo_diffraction_qt.png 
   
 
Welcome to diffraction_qt's documentation!
==========================================


diffraction_qt is an open source python toolkit for graphical representations of diffraction phenomena. 
Its main objective is to help understand how the parameters of the experimental setup (aperture size, 
aperture shape, laser wavelength, focal length, etc) influence the diffraction pattern.
So diffraction_qt is made up of an assembly of interactive interfaces which allow the user to observe t
he evolution of the diffraction pattern when he changes the various parameters..
This website is a support for the theoretical knowledge necessary in this field of physics and also 
to explain the operating principle of the different interface windows.

You can clone the codes from `gitlab <https://gitlab.com/diffraction_qt/diffraction_qt/-/tree/master/>`_

Please send comments, suggestions and report bugs by mail or via the `gitlab issue tracker <https://gitlab.com/diffraction_qt/diffraction_qt/-/issues>`_


.. figure:: _static/dessin_1ere_page.png
..    :align: left

diffraction_qt documentation
--------------------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   principle_diffraction
   square_hole
   double_slit
	

References
--------------------------------------

In French :

* José-Philippe Pérez, Optique : Fondements et applications, Dunod, 7e éd.

In English :

* Goodman Joseph, 2005, Introduction to Fourier Optics, Roberts & Co
* Hecht Eugene, Optics, 2002, Addison Wesley

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
