"""
Created on Thu Oct  8 18:10:51 2020

@author: majorel
"""


from __future__ import print_function, division

import diffraction_double_slit_ui as ui

import numpy as np

from PyQt5.QtWidgets import QMainWindow, QApplication

import time

class Exploit_Results(ui.Ui_MainWindow):
    """ Main class with the graphic aspect of the exploitResults_ui script.
    Gives life to the buttons by attributing functions to them """
    def __init__(self):
        """ Call the window, connect functions """
        app = QApplication(sys.argv)
        MainWindow = QMainWindow()
        self.setupUi(MainWindow)
        MainWindow.show()
        self.plot_condition = False
        self.plot_condition_2 = False
        self.a = self.numberA_SpinBox.value()
        self.b = self.numberB_SpinBox.value()
        self.d = self.numberD_SpinBox.value()
        self.NX = self.numberNX_SpinBox.value()
        self.NY = self.numberNY_SpinBox.value()
        self.lamda = self.numberlamda_SpinBox.value()
        self.focal = self.numberfocal_SpinBox.value()
        self.set_values()
        # Following objects are lists so we have one for each main tab
        # Link physical objects (buttons, ..) with actions
        self.connect_function()

        # Edit Plot
        sys.exit(app.exec_())

    def connect_function(self):
        """ Connects events of the window items with actions """
        # Load files buttons
        self.plotButton.clicked.connect(self.plot_fentes_diffractif)
        self.plotButton.clicked.connect(self.plot_figure_diffraction)
        self.plotButton.clicked.connect(self.plot_line_diffraction)
        self.plotButton.clicked.connect(self.plot_zoom_line_diffraction)
        self.checkBoxContrast.clicked.connect(self.Check_plot)
        self.checkBoxsincVB.clicked.connect(self.Check_plot)
        self.checkBoxplotsincUA.clicked.connect(self.Check_plot_2)
        self.checkBoxplotcosUD.clicked.connect(self.Check_plot_2)
        self.numberA_SpinBox.valueChanged.connect(self.set_values)
        self.numberB_SpinBox.valueChanged.connect(self.set_values)
        self.numberD_SpinBox.valueChanged.connect(self.set_values)
        self.numberNX_SpinBox.valueChanged.connect(self.set_values)
        self.numberNY_SpinBox.valueChanged.connect(self.set_values)
        self.numberlamda_SpinBox.valueChanged.connect(self.set_values)
        self.numberfocal_SpinBox.valueChanged.connect(self.set_values)

    def set_values(self):
        """set values of variables used from what is put in software"""
        self.a = self.numberA_SpinBox.value()*1e-6
        self.b = self.numberB_SpinBox.value()*self.a
        self.d = self.numberD_SpinBox.value()*self.a
        self.NX = self.numberNX_SpinBox.value()
        self.NY = self.numberNY_SpinBox.value()
        self.lamda = self.numberlamda_SpinBox.value()*1e-9
        self.focal = self.numberfocal_SpinBox.value()*1e-2

        ## --- function to define the interest area in Fourier plane ( from x_min to x_max and from y_min to y_max)
    def pos(self, x_min, x_max, y_min, y_max, NX, NY):
        X,Y = np.meshgrid(np.linspace(x_min,x_max,NX),np.linspace(y_min,y_max,NY))
        MAP = np.array([X.T, Y.T]).T
        return MAP
    
    ## --- function plotting the double slit shape
    def fentes(self):
        
    ## window size of the pierced screen
        x_min = -30.*self.a ; y_min = -30.*self.b       
        x_max = 30.*self.a ; y_max = 30.*self.b
    ## conditions for the same window dimensions along Ox and Oy  
        if self.a>self.b:
            x_min+=30*(self.a-self.b); x_max-=30*(self.a-self.b)
        elif self.b>self.a:
            y_min+=30*(self.b-self.a); y_max-=30*(self.b-self.a)
        
        NX = NY = 250
        self.val_map=[]
        for yi in np.linspace(y_min,y_max,NY):
            for xi in np.linspace(x_min,x_max,NX):
                if -self.d/2.-self.a/2.<xi<-self.d/2.+self.a/2. and abs(yi)<self.b/2.: ## condition slit 1
                    self.val_map.append(1)               
                elif self.d/2.-self.a/2.<xi<self.d/2.+self.a/2. and abs(yi)<self.b/2.: ## condition slit 2
                    self.val_map.append(1)               
                else :
                    self.val_map.append(0)
        self.val_map = np.array(np.reshape(self.val_map, (NX, NY)))
        
        return self.val_map
    
    ## --- Intensity function (https://en.wikipedia.org/wiki/Double-slit_experiment)
    def Intensite(self, MAP):
        u = MAP.T[0]/self.lamda/self.focal
        v = MAP.T[1]/self.lamda/self.focal
        if self.checkBoxsincVB.isChecked():
            I=np.sinc(u*self.a)**2*np.sinc(v*self.b)**2*np.cos(np.pi*u*self.d)**2
        else:
            I=np.sinc(u*self.a)**2*np.cos(np.pi*u*self.d)**2
        return I

    ## -- function to active the checkbox after plot only
    def Check_plot(self):
        if self.plot_condition:
            self.plot_figure_diffraction()

    ## -- function to active the checkbox after plot only
    def Check_plot_2(self):
        if self.plot_condition_2:
            self.plot_zoom_line_diffraction()

    def plot_fentes_diffractif(self):
        """ Plot a 2D view of the double slit.
            """
        self.trouCurveMatplotlibwidget.figure.axes[0].clear()
        
        trou_diff = self.fentes()
        
        self.trouCurveMatplotlibwidget.figure.axes[0].imshow(trou_diff, cmap = 'jet', aspect='equal')
        self.trouCurveMatplotlibwidget.figure.axes[0].set_title("Figure double slit")
        self.trouCurveMatplotlibwidget.figure.axes[0].set_xlabel("X axis")
        self.trouCurveMatplotlibwidget.figure.axes[0].set_ylabel("Y axis")
        self.trouCurveMatplotlibwidget.figure.axes[0].set_xticks([])
        self.trouCurveMatplotlibwidget.figure.axes[0].set_yticks([])
        self.trouCurveMatplotlibwidget.draw()



    def plot_figure_diffraction(self):
        """ Plot diffraction pattern in the Fourier plane.
            """

        self.plot_condition = True
        self.diffCurveMatplotlibwidget.figure.axes[0].clear()
        
        self.x_min = self.y_min = -5e-3          ## -- min values along Ox and Oy on the Fourier plane
        self.x_max = self.y_max = 5e-3          ## -- max values along Ox and Oy on the Fourier plane
        
        ## --- set of ([x, y]) positions where we compute the intensity
        if self.NY%2==0:
            self.NY+=1
        self.positions = self.pos(self.x_min, self.x_max, self.y_min, self.y_max, self.NX, self.NY)
        
        
        self.I = self.Intensite(self.positions)
        
        X = np.reshape(self.I, (self.NX, self.NY)).T

        if self.checkBoxsincVB.isChecked():
            pass
        else:
            self.diffCurveMatplotlibwidget.figure.axes[0].hlines(0., self.x_min, self.x_max, color='C0', linestyles='dashed')
        
        
        if self.checkBoxContrast.isChecked():
            im = self.diffCurveMatplotlibwidget.figure.axes[0].imshow(X, 
                                                                      aspect='auto', cmap='hot', 
                                                                      vmin=np.min(X), vmax=np.max(X)-0.9*np.max(X), 
                                                                      extent=[self.x_min,self.x_max,self.y_min,self.y_max])
            cb = self.diffCurveMatplotlibwidget.figure.colorbar(im, ticks=[0, 0.1])
        else :
            im = self.diffCurveMatplotlibwidget.figure.axes[0].imshow(X, 
                                                                      aspect='auto', cmap='hot', 
                                                                      vmin=np.min(X), vmax=np.max(X), 
                                                                      extent=[self.x_min,self.x_max,self.y_min,self.y_max])
            cb = self.diffCurveMatplotlibwidget.figure.colorbar(im, ticks=[0, 1])
        self.diffCurveMatplotlibwidget.figure.axes[0].set_title("Diffraction pattern")
        cb.set_label("Normalized intensity")
        
        self.diffCurveMatplotlibwidget.figure.axes[0].set_xlabel("X positions (m)")
        self.diffCurveMatplotlibwidget.figure.axes[0].set_ylabel("Y positions (m)")
        self.diffCurveMatplotlibwidget.figure.axes[0].locator_params(nbins=6)
        self.diffCurveMatplotlibwidget.draw()
        cb.remove()
                


    def plot_line_diffraction(self):
        """ Plot diffraction pattern curve along the Oy=0 axis.
            """

        
        self.lineCurveMatplotlibwidget.figure.axes[0].clear()
        
        ## --- extraction of I along Oy=0 axis
        indx = np.where(self.positions.T[1]==0)
        
        x_line = self.positions.T[0][indx]
        I_line = self.I[indx]
        
        self.lineCurveMatplotlibwidget.figure.axes[0].plot(x_line, I_line)
        self.lineCurveMatplotlibwidget.figure.axes[0].set_title("Diffraction pattern curve along Oy=0")
        self.lineCurveMatplotlibwidget.figure.axes[0].set_xlabel("X positions (m)")
        self.lineCurveMatplotlibwidget.figure.axes[0].set_ylabel("Normalized intensity")
        self.lineCurveMatplotlibwidget.figure.axes[0].locator_params(nbins=6)
        self.lineCurveMatplotlibwidget.draw()
        
        
                
    def plot_zoom_line_diffraction(self):
        """ Plot diffracton pattern curve along the Oy=0 axis (smaller x-axis range).
            """
        self.plot_condition_2 = True
        self.linezoomCurveMatplotlibwidget.figure.axes[0].clear()
        
        ## --- extraction of I along Oy=0 axis
        indx = np.where(self.positions.T[1]==0)
        
        x_line = self.positions.T[0][indx]
        I_line = self.I[indx]
        
        ## -- modulation sinus cardinal
        sinc_ua = np.sinc(self.positions.T[0][indx]*self.a/self.lamda/self.focal)
        ## -- modulation cosinus
        cos_ud = np.cos(np.pi*self.positions.T[0][indx]/self.lamda/self.focal*self.d)
        
        ylim_min=0.0
        ylim_max=0.05
        self.linezoomCurveMatplotlibwidget.figure.axes[0].plot(x_line, I_line, label='I')
        if self.checkBoxplotsincUA.isChecked():
            self.linezoomCurveMatplotlibwidget.figure.axes[0].plot(x_line, sinc_ua**2, color='C1', label=r'sinc$^2$')
        if self.checkBoxplotcosUD.isChecked():
            self.linezoomCurveMatplotlibwidget.figure.axes[0].plot(x_line, cos_ud**2*(ylim_max-1./5*ylim_max), color='C2', label=r'cos$^2$')
        self.linezoomCurveMatplotlibwidget.figure.axes[0].set_ylim([ylim_min, ylim_max])
        self.linezoomCurveMatplotlibwidget.figure.axes[0].set_xlim([0.7*self.x_min, 0.7*self.x_max])
        self.linezoomCurveMatplotlibwidget.figure.axes[0].set_title("Diffraction pattern curve along Oy=0 (zoom)")
        self.linezoomCurveMatplotlibwidget.figure.axes[0].set_xlabel("X positions (m)")
        # self.linezoomCurveMatplotlibwidget.figure.axes[0].locator_params(nbins=6)
        self.linezoomCurveMatplotlibwidget.figure.axes[0].legend(loc='best')
        self.linezoomCurveMatplotlibwidget.figure.axes[0].set_ylabel("Normalized intensity")
        self.linezoomCurveMatplotlibwidget.draw()
        
        
        

if __name__ == "__main__":
    import sys
    Exploit_Results()